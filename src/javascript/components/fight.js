import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const firstInitialHealth = firstFighter.health;
    const secondInitialHealth = secondFighter.health;

    let firstIsAbleToCombo = true;
    let secondIsAbleToCombo = true;

    const firstFighterHealthIndicator = document.querySelector('#left-fighter-indicator');
    const secondFighterHealthIndicator = document.querySelector('#right-fighter-indicator');

    const indicateHealth = (el, currentValue, initialValue) => {
      el.style.width = `${Math.trunc((currentValue / initialValue) * 100)}%`;
    }

    const firstHitCombination = () => {
      if (!firstIsAbleToCombo) {
        console.log('Wait!');
        return;
      }
      firstIsAbleToCombo = false;
      secondFighter.health -= getHitPower(firstFighter, true);
      indicateHealth(secondFighterHealthIndicator, secondFighter.health, secondInitialHealth);
      if (secondFighter.health < 0) {
        return resolve (firstFighter);
      }
      setTimeout(() => {
        firstIsAbleToCombo = true;
      }, 10000);
    };
    const secondHitCombination = () => {
      if (!secondIsAbleToCombo) {
        console.log('Wait!');
        return;
      }
      secondIsAbleToCombo = false;
      firstFighter.health -= getHitPower(secondFighter, true);
      indicateHealth(firstFighterHealthIndicator, firstFighter.health, firstInitialHealth);
      if (firstFighter.health < 0) {
        return resolve (secondFighter);
      }
      setTimeout(() => {
        secondIsAbleToCombo = true;
      }, 10000);
    };

    const firstAttackSecondBlock = () => {
      const damage = getDamage(firstFighter, secondFighter);
      if (damage) {
        secondFighter.health -= damage;
        indicateHealth(secondFighterHealthIndicator, secondFighter.health, secondInitialHealth);
      }
      if (secondFighter.health < 0) {
        return resolve (firstFighter);
      }
    };

    const secondAttackFirstBlock = () => {
      const damage = getDamage(secondFighter, firstFighter);
      if (damage) {
        firstFighter.health -= damage;
        indicateHealth(firstFighterHealthIndicator, firstFighter.health, firstInitialHealth);
      }
      if (firstFighter.health < 0) {
        return resolve (secondFighter);
      }
    };

    const firstAttack = () => {
      secondFighter.health -= getHitPower(firstFighter);
      indicateHealth(secondFighterHealthIndicator, secondFighter.health, secondInitialHealth);
      if (secondFighter.health < 0) {
        return resolve (firstFighter);
      }
    };

    const secondAttack = () => {
      firstFighter.health -= getHitPower(secondFighter);
      indicateHealth(firstFighterHealthIndicator, firstFighter.health, firstInitialHealth);
      if (firstFighter.health < 0) {
        return resolve (secondFighter);
      }
    };

    const isRun = (keysPressed, keyCodes) => {
      for (let code of keyCodes) {
        if (!keysPressed.has(code)) {
          return false;
        }
      }
      return true;
    }

    // eventListeners

    let pressed = new Set();
    document.addEventListener('keydown', (event) => {
      pressed.add(event.code);
      if (isRun(pressed, controls.PlayerOneCriticalHitCombination)) {
        firstHitCombination();
      }
      if (isRun(pressed, controls.PlayerTwoCriticalHitCombination)) {
        secondHitCombination();
      }
      if (isRun(pressed, [controls.PlayerOneAttack, controls.PlayerTwoBlock])) {
        firstAttackSecondBlock();
      }
      if (isRun(pressed, [controls.PlayerTwoAttack, controls.PlayerOneBlock])) {
        secondAttackFirstBlock();
      }
      if (pressed.has(controls.PlayerOneAttack) && pressed.size === 1) {
        firstAttack();
      }
      if (pressed.has(controls.PlayerTwoAttack) && pressed.size === 1) {
        secondAttack();
      }

    });
    document.addEventListener('keyup', function(event) {
      pressed.delete(event.code);
    });




    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter, superHit = false) {
  // return hit power
  return superHit ? fighter.attack * 2 : fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}
