import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const bodyElement = createElement({ tagName: 'div'});
  bodyElement.appendChild(createFighterImage(fighter));
  const info = createElement({ tagName: 'div'});
  info.innerText = `${fighter.name} | health ${fighter.health}`;
  bodyElement.appendChild(info);
  const title = `${fighter.name} won!`
  showModal({title, bodyElement});
}
